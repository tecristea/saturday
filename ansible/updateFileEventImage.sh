#!/bin/bash

docker run --rm -it \
	-v $(pwd):/ansible \
	-v ~/.ssh/id_ed25519_prolion:/root/id_ed25519 \
	prolion/ansible \
	ansible loadvms \
	-a "docker pull dev-registry-docker.prolion.com/prolion/cryptospike/cs-java-services/file-event-service-bundle:develop" \
	-i inventory.yaml
