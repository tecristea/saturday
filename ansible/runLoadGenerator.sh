#!/bin/bash

docker run --rm -it \
	-v $(pwd):/ansible \
	-v ~/.ssh/id_ed25519_prolion:/root/id_ed25519 \
	prolion/ansible \
	ansible-playbook \
	./playbooks/run_load_generator.yml \
	-i inventory.yaml
