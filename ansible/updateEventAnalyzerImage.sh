#!/bin/bash

docker run --rm -it \
	-v $(pwd):/ansible \
	-v ~/.ssh/id_ed25519_prolion:/root/id_ed25519 \
	prolion/ansible \
	ansible all \
	-a "docker pull dev-registry-docker.prolion.com/prolion/cryptospike/cs-java-services/event-analyzer:develop" \
	-i inventory.yaml
