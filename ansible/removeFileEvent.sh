#!/bin/bash

docker run --rm -it \
	-v $(pwd):/ansible \
	-v ~/.ssh/id_ed25519_prolion:/root/id_ed25519 \
	prolion/ansible \
	ansible-playbook \
	./playbooks/remove_file_event.yml \
	-i inventory.yaml
