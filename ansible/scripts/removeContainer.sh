#!/bin/bash

SERVICE_NAME=${1}

docker rm -f \
  $(docker ps --filter label=com.docker.swarm.service.name=${1} --filter status=running --format="{{.ID}}")

