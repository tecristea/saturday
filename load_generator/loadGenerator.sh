#!/bin/bash

CRYPTO_SPIKE_IP="10.123.12.36,10.123.12.37,10.123.12.38,10.123.12.39,10.123.12.40,10.123.12.41,10.123.12.42,10.123.12.43"
CRYPTO_SPIKE_PORT=18080
CONNECTIONS_PER_SERVER=2
MAXIMUM_NUMBER_OF_NOTIFICATIONS_PER_FILE_ACTION=5
STEP_DURATION_MINUTES=5
STEP_LOAD_NOTIFICATIONS_PER_SECOND=70000
STEP_WARM_UP_DURATION_SECONDS=120
NUMBER_OF_STEPS=1
USERS_COUNT=1
CONNECTION_SYNC_MODE=true
SEND_BUFFER=12582912
WRITE_BUFFER_HIGH_WATER_MARK=12582912

docker run \
        -it \
        -p 9020:9020 \
        -p 8080:8080 \
        --name netapp-fpolicy-event-generator \
        -d dev-registry-docker.prolion.com/prolion/netapp-services/netapp-fpolicy-event-generator:feature-roundtrip-sampling \
        java -Xmx24G -jar \
        -Djava.rmi.server.hostname=10.0.0.10 \
        -Dcom.sun.management.jmxremote=true \
        -Dcom.sun.management.jmxremote.authenticate=false \
        -Dcom.sun.management.jmxremote.local.only=false \
        -Dcom.sun.management.jmxremote.port=9020 \
        -Dcom.sun.management.jmxremote.rmi.port=9020 \
        -Dcom.sun.management.jmxremote.ssl=false \
        -Dapplication.fpolicy-event.users-count=${USERS_COUNT} \
        -Dapplication.netty.send-buffer=${SEND_BUFFER} \
        -Dapplication.netty.write-buffer-high-water-mark=${WRITE_BUFFER_HIGH_WATER_MARK} \
        -Dapplication.fpolicy-event.locations[0].server=svm_andrei \
        -Dapplication.fpolicy-event.locations[0].volume-msid=2149972988 \
        -Dapplication.fpolicy-event.locations[0].share=vol_cifs \
        -Dapplication.fpolicy-event-generator.fpolicy-server.ip=${CRYPTO_SPIKE_IP} \
        -Dapplication.fpolicy-event-generator.fpolicy-server.port=${CRYPTO_SPIKE_PORT} \
        -Dapplication.fpolicy-event-generator.connection-sync-mode=${CONNECTION_SYNC_MODE} \
        -Dapplication.fpolicy-event-generator.connections-per-server=${CONNECTIONS_PER_SERVER} \
        -Dapplication.fpolicy-event-generator.max-number-of-notifications-per-file-action=${MAXIMUM_NUMBER_OF_NOTIFICATIONS_PER_FILE_ACTION} \
        -Dapplication.fpolicy-event-generator.number-of-steps=${NUMBER_OF_STEPS} \
        -Dapplication.fpolicy-event-generator.step-duration-minutes=${STEP_DURATION_MINUTES} \
        -Dapplication.fpolicy-event-generator.step-load-notifications-per-second=${STEP_LOAD_NOTIFICATIONS_PER_SECOND} \
        -Dapplication.fpolicy-event-generator.step-warm-up-duration-seconds=${STEP_WARM_UP_DURATION_SECONDS} \
        exec.jar

