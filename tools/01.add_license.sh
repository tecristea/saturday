#!/bin/bash

# set -x

IP=localhost
ADMIN_KEY='change_me'

BASE_URL=http://${IP}:8081
BASE_URL=https://${IP}
PORT=443

# add license in admin-service
curl -k -X POST "http://${IP}:3000/api/v1/admin/license?ip=${IP}&port=${PORT}"
	-H "accept: application/json"   \
	-H "Content-Type: application/json" \
 	-d "{\"key\":\"${ADMIN_KEY}\"}"

# force cluster management to load license
curl -k -X PATCH "${BASE_URL}/api/v1/server/cluster/licenses" \
	-H  "accept: application/json"
