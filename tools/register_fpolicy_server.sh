#!/bin/bash

# https://stackoverflow.com/questions/43373176/store-json-directly-in-bash-script-with-variables/43373520

# set -x

if [[ "$#" -ne 1 ]]; then
    echo "Usage: $0 <cs_ip_address>"
    exit 0
fi

IP=${1}
echo "Using ip: ${IP}"

IDENTIFIER=test
ENGINE_NAME=cs_${IDENTIFIER}_ACTIVE_engine
FPOLICY_PORT=18080
POLICY_NAME=cs_${IDENTIFIER}_ACTIVE_policy
SERVER_NAME=svm_${IDENTIFIER}
CLUSTER_HOST=192.168.0.22

BASE_URL=http://${IP}:8081
BASE_URL=https://${IP}

CLUSTER_ID=$(curl -skX GET "${BASE_URL}/api/v1/server/cluster/clusters" \
  -H  "accept: application/json" \
  | jq -r ".[] | select(.connection.host == \"${CLUSTER_HOST}\") | .id")

echo "CLUSTER_ID=${CLUSTER_ID}"

SERVER_IDENTIFIER=$(curl -s -k \
	-X GET "${BASE_URL}/api/v1/server/cluster/volumes/all/connection" \
	-H "accept: application/json" \
	| jq -r "[.[] | {server,connection} | select(.server.name==\"${SERVER_NAME}\" and .connection.host==\"${CLUSTER_HOST}\")][0] | .server.identifier")

echo "SERVER_IDENTIFIER=${SERVER_IDENTIFIER}"

register_json=$(cat <<EOF
{
    "engine": {
        "name": "${ENGINE_NAME}",
        "port": ${FPOLICY_PORT},
        "primaryServers": [
            "${IP}"
        ],
        "secondaryServers": [],
        "type": "ACTIVE"
    },
    "policies": [
        {
            "enabled": true,
            "engine": {
                "name": "${ENGINE_NAME}",
                "port": ${FPOLICY_PORT},
                "primaryServers": [
                    "${IP}"
                ],
                "secondaryServers": [],
                "type": "ACTIVE"
            },
            "events": [
                {
                    "filters": {
                        "closeWithModification": true,
                        "closeWithRead": true,
                        "firstRead": true,
                        "firstWrite": true,
                        "openWithDeleteIntent": true,
                        "openWithWriteIntent": true
                    },
                    "name": "cs_${IDENTIFIER}_event_cifs",
                    "operations": {
                        "close": true,
                        "create": true,
                        "delete": true,
                        "open": true,
                        "read": true,
                        "rename": true,
                        "write": true
                    },
                    "protocol": "cifs",
                    "volumeMonitoring": false
                }
            ],
            "mandatory": true,
            "name": "${POLICY_NAME}_cifs",
            "priority": 1,
            "scopes": {
                "includeExportPolicies": [
                    "*"
                ],
                "includeShares": [
                    "*"
                ],
                "includeVolumes": [
                    "*"
                ]
            },
            "serverIdentifier": "${SERVER_IDENTIFIER}",
            "serverName": "${SERVER_NAME}"
        },
        {
            "enabled": true,
            "engine": {
                "name": "${ENGINE_NAME}",
                "port": ${FPOLICY_PORT},
                "primaryServers": [
                    "${IP}"
                ],
                "secondaryServers": [],
                "type": "ACTIVE"
            },
            "events": [
                {
                    "filters": {
                        "firstRead": true,
                        "firstWrite": true
                    },
                    "name": "cs_${IDENTIFIER}_event_nfsv3",
                    "operations": {
                        "create": true,
                        "createDir": true,
                        "delete": true,
                        "deleteDir": true,
                        "link": true,
                        "lookup": true,
                        "read": true,
                        "rename": true,
                        "renameDir": true,
                        "symlink": true,
                        "write": true
                    },
                    "protocol": "nfsv3",
                    "volumeMonitoring": false
                }
            ],
            "mandatory": true,
            "name": "${POLICY_NAME}_nfsv3",
            "priority": 1,
            "scopes": {
                "includeExportPolicies": [
                    "*"
                ],
                "includeShares": [
                    "*"
                ],
                "includeVolumes": [
                    "*"
                ]
            },
            "serverIdentifier": "${SERVER_IDENTIFIER}",
            "serverName": "${SERVER_NAME}"
        },
        {
            "enabled": true,
            "engine": {
                "name": "${ENGINE_NAME}",
                "port": ${FPOLICY_PORT},
                "primaryServers": [
                    "${IP}"
                ],
                "secondaryServers": [],
                "type": "ACTIVE"
            },
            "events": [
                {
                    "filters": {
                        "firstRead": true,
                        "firstWrite": true
                    },
                    "name": "cs_${IDENTIFIER}_event_nfsv4",
                    "operations": {
                        "create": true,
                        "createDir": true,
                        "delete": true,
                        "deleteDir": true,
                        "link": true,
                        "lookup": true,
                        "read": true,
                        "rename": true,
                        "renameDir": true,
                        "symlink": true,
                        "write": true
                    },
                    "protocol": "nfsv4",
                    "volumeMonitoring": false
                }
            ],
            "mandatory": true,
            "name": "${POLICY_NAME}_nfsv4",
            "priority": 1,
            "scopes": {
                "includeExportPolicies": [
                    "*"
                ],
                "includeShares": [
                    "*"
                ],
                "includeVolumes": [
                    "*"
                ]
            },
            "serverIdentifier": "${SERVER_IDENTIFIER}",
            "serverName": "${SERVER_NAME}"
        }
    ],
    "serverIdentifier": "${SERVER_IDENTIFIER}",
    "serverName": "${SERVER_NAME}"
}
EOF
)

curl -k -X POST "${BASE_URL}/api/v1/server/cluster/fileevent/${CLUSTER_ID}/register" \
	-H  "accept: */*" \
     	-H  "Content-Type: application/json"  \
	-d "${register_json}"
