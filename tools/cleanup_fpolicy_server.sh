#!/bin/bash

if [[ "$#" -ne 1 ]]; then
    echo "Usage: $0 <svm>"
    exit 0
fi

SERVER_NAME=${1}

# set -x
IP=10.0.10.31
IP=localhost
BASE_URL=http://${IP}:8081
BASE_URL=https://${IP}
CLUSTER_HOST=192.168.0.22

echo "getting server name & server id from cluster management"
SERVER_ID=$(curl -s -k \
	-X GET "${BASE_URL}/api/v1/server/cluster/volumes/all/connection" \
	-H  "accept: application/json" \
	| jq -r "[.[] | {server,connection} | select(.server.name==\"${SERVER_NAME}\" and .connection.host==\"${CLUSTER_HOST}\")][0] | .server.id")

echo "cleanup registration for SERVER_NAME=${SERVER_NAME} & SERVER_ID=${SERVER_ID}"

curl -k -X POST "${BASE_URL}/api/v1/server/cluster/fileevent/${SERVER_ID}/cleanup" \
	-H  "accept: */*" \
	-d ""
