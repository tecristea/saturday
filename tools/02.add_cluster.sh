#!/bin/bash

# set -x

IP=localhost
ONTAP_PASSWORD=password
ONTAP_USERNAME=username
CLUSTER_HOST=192.168.0.22

BASE_URL=http://${IP}:8081
BASE_URL=https://${IP}

# add ONTAP cluster in cluster-management
curl -k -X \
	POST "${BASE_URL}/api/v1/server/cluster/clusters" \
	-H  "accept: application/json"    \
	-H  "Content-Type: application/json"    \
	-d "{\"host\":\"${CLUSTER_HOST}\",\"password\":\"${ONTAP_PASSWORD}\",\"platform\":\"ONTAP\",\"username\":\"${ONTAP_USERNAME}\"}"

CLUSTER_ID=$(curl -skX GET "${BASE_URL}/api/v1/server/cluster/clusters" \
  	-H  "accept: application/json" \
  	| jq -r ".[] | select(.connection.host == \"${CLUSTER_HOST}\") | .id")

echo "reload cluster with id: ${CLUSTER_ID}"
 
# reload cluster data from ONTAP
curl -k -X GET \
	"${BASE_URL}/api/v1/server/cluster/clusters/${CLUSTER_ID}/tree?reload=true" \
	-H  "accept: application/json"
